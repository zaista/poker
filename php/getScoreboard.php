<?php

    function connect($xml) {
        // create connection
        $mysqli = new mysqli($xml->database->hostname, $xml->database->username, $xml->database->password, $xml->database->database);
        // check connection
        if ($mysqli->connect_error) {
            die("Connection failed: " . $mysqli->connect_error);
        }
        
        // this will make sure cyrilic letters are displayed properly
        $mysqli->query("SET NAMES utf8");
        
        return $mysqli;
    }
    
    function get_config($config) {
        // load configuration file
        $xml = simplexml_load_file($config) or die("Error: Cannot load configuration file");
        return $xml;
    }
    
    $xml = get_config('private/config.xml');
    $mysqli = connect($xml);

	$stmt = $mysqli->prepare('SELECT Name, Score FROM scoreboard ORDER BY score DESC LIMIT 10');

	$stmt->execute();
	$result = $stmt->get_result();

	if ($result->num_rows > 0) {

		$scoreboard;
		while($row = $result->fetch_row()) {
			$scoreboard[] = $row;
		}
		echo json_encode($scoreboard);
	} else {
		echo "failed";
	}
?>
